/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LB = 258,
     RB = 259,
     LP = 260,
     RP = 261,
     MENUS = 262,
     MENU = 263,
     BUTTON = 264,
     DEFAULT_FUNCTION = 265,
     PLUS = 266,
     MINUS = 267,
     ALL = 268,
     OR = 269,
     CURSORS = 270,
     PIXMAPS = 271,
     ICONS = 272,
     COLOR = 273,
     SAVECOLOR = 274,
     MONOCHROME = 275,
     FUNCTION = 276,
     ICONMGR_SHOW = 277,
     ICONMGR = 278,
     ALTER = 279,
     WINDOW_FUNCTION = 280,
     ZOOM = 281,
     ICONMGRS = 282,
     ICONMGR_GEOMETRY = 283,
     ICONMGR_NOSHOW = 284,
     MAKE_TITLE = 285,
     ICONIFY_BY_UNMAPPING = 286,
     DONT_ICONIFY_BY_UNMAPPING = 287,
     NO_BORDER = 288,
     NO_ICON_TITLE = 289,
     NO_TITLE = 290,
     AUTO_RAISE = 291,
     NO_HILITE = 292,
     ICON_REGION = 293,
     WINDOW_REGION = 294,
     META = 295,
     SHIFT = 296,
     LOCK = 297,
     CONTROL = 298,
     WINDOW = 299,
     TITLE = 300,
     ICON = 301,
     ROOT = 302,
     FRAME = 303,
     COLON = 304,
     EQUALS = 305,
     SQUEEZE_TITLE = 306,
     DONT_SQUEEZE_TITLE = 307,
     START_ICONIFIED = 308,
     NO_TITLE_HILITE = 309,
     TITLE_HILITE = 310,
     MOVE = 311,
     RESIZE = 312,
     WAITC = 313,
     SELECT = 314,
     KILL = 315,
     LEFT_TITLEBUTTON = 316,
     RIGHT_TITLEBUTTON = 317,
     NUMBER = 318,
     KEYWORD = 319,
     NKEYWORD = 320,
     CKEYWORD = 321,
     CLKEYWORD = 322,
     FKEYWORD = 323,
     FSKEYWORD = 324,
     SKEYWORD = 325,
     DKEYWORD = 326,
     JKEYWORD = 327,
     WINDOW_RING = 328,
     WINDOW_RING_EXCLUDE = 329,
     WARP_CURSOR = 330,
     ERRORTOKEN = 331,
     NO_STACKMODE = 332,
     ALWAYS_ON_TOP = 333,
     WORKSPACE = 334,
     WORKSPACES = 335,
     WORKSPCMGR_GEOMETRY = 336,
     OCCUPYALL = 337,
     OCCUPYLIST = 338,
     MAPWINDOWCURRENTWORKSPACE = 339,
     MAPWINDOWDEFAULTWORKSPACE = 340,
     UNMAPBYMOVINGFARAWAY = 341,
     OPAQUEMOVE = 342,
     NOOPAQUEMOVE = 343,
     OPAQUERESIZE = 344,
     NOOPAQUERESIZE = 345,
     DONTSETINACTIVE = 346,
     CHANGE_WORKSPACE_FUNCTION = 347,
     DEICONIFY_FUNCTION = 348,
     ICONIFY_FUNCTION = 349,
     AUTOSQUEEZE = 350,
     STARTSQUEEZED = 351,
     DONT_SAVE = 352,
     AUTO_LOWER = 353,
     ICONMENU_DONTSHOW = 354,
     WINDOW_BOX = 355,
     IGNOREMODIFIER = 356,
     WINDOW_GEOMETRIES = 357,
     ALWAYSSQUEEZETOGRAVITY = 358,
     VIRTUAL_SCREENS = 359,
     IGNORE_TRANSIENT = 360,
     DONTTOGGLEWORKSPACEMANAGERSTATE = 361,
     STRING = 362
   };
#endif
#define LB 258
#define RB 259
#define LP 260
#define RP 261
#define MENUS 262
#define MENU 263
#define BUTTON 264
#define DEFAULT_FUNCTION 265
#define PLUS 266
#define MINUS 267
#define ALL 268
#define OR 269
#define CURSORS 270
#define PIXMAPS 271
#define ICONS 272
#define COLOR 273
#define SAVECOLOR 274
#define MONOCHROME 275
#define FUNCTION 276
#define ICONMGR_SHOW 277
#define ICONMGR 278
#define ALTER 279
#define WINDOW_FUNCTION 280
#define ZOOM 281
#define ICONMGRS 282
#define ICONMGR_GEOMETRY 283
#define ICONMGR_NOSHOW 284
#define MAKE_TITLE 285
#define ICONIFY_BY_UNMAPPING 286
#define DONT_ICONIFY_BY_UNMAPPING 287
#define NO_BORDER 288
#define NO_ICON_TITLE 289
#define NO_TITLE 290
#define AUTO_RAISE 291
#define NO_HILITE 292
#define ICON_REGION 293
#define WINDOW_REGION 294
#define META 295
#define SHIFT 296
#define LOCK 297
#define CONTROL 298
#define WINDOW 299
#define TITLE 300
#define ICON 301
#define ROOT 302
#define FRAME 303
#define COLON 304
#define EQUALS 305
#define SQUEEZE_TITLE 306
#define DONT_SQUEEZE_TITLE 307
#define START_ICONIFIED 308
#define NO_TITLE_HILITE 309
#define TITLE_HILITE 310
#define MOVE 311
#define RESIZE 312
#define WAITC 313
#define SELECT 314
#define KILL 315
#define LEFT_TITLEBUTTON 316
#define RIGHT_TITLEBUTTON 317
#define NUMBER 318
#define KEYWORD 319
#define NKEYWORD 320
#define CKEYWORD 321
#define CLKEYWORD 322
#define FKEYWORD 323
#define FSKEYWORD 324
#define SKEYWORD 325
#define DKEYWORD 326
#define JKEYWORD 327
#define WINDOW_RING 328
#define WINDOW_RING_EXCLUDE 329
#define WARP_CURSOR 330
#define ERRORTOKEN 331
#define NO_STACKMODE 332
#define ALWAYS_ON_TOP 333
#define WORKSPACE 334
#define WORKSPACES 335
#define WORKSPCMGR_GEOMETRY 336
#define OCCUPYALL 337
#define OCCUPYLIST 338
#define MAPWINDOWCURRENTWORKSPACE 339
#define MAPWINDOWDEFAULTWORKSPACE 340
#define UNMAPBYMOVINGFARAWAY 341
#define OPAQUEMOVE 342
#define NOOPAQUEMOVE 343
#define OPAQUERESIZE 344
#define NOOPAQUERESIZE 345
#define DONTSETINACTIVE 346
#define CHANGE_WORKSPACE_FUNCTION 347
#define DEICONIFY_FUNCTION 348
#define ICONIFY_FUNCTION 349
#define AUTOSQUEEZE 350
#define STARTSQUEEZED 351
#define DONT_SAVE 352
#define AUTO_LOWER 353
#define ICONMENU_DONTSHOW 354
#define WINDOW_BOX 355
#define IGNOREMODIFIER 356
#define WINDOW_GEOMETRIES 357
#define ALWAYSSQUEEZETOGRAVITY 358
#define VIRTUAL_SCREENS 359
#define IGNORE_TRANSIENT 360
#define DONTTOGGLEWORKSPACEMANAGERSTATE 361
#define STRING 362




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 121 "gram.y"
typedef union YYSTYPE {
    int num;
    char *ptr;
} YYSTYPE;
/* Line 1285 of yacc.c.  */
#line 256 "y.tab.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



