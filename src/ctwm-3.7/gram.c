/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LB = 258,
     RB = 259,
     LP = 260,
     RP = 261,
     MENUS = 262,
     MENU = 263,
     BUTTON = 264,
     DEFAULT_FUNCTION = 265,
     PLUS = 266,
     MINUS = 267,
     ALL = 268,
     OR = 269,
     CURSORS = 270,
     PIXMAPS = 271,
     ICONS = 272,
     COLOR = 273,
     SAVECOLOR = 274,
     MONOCHROME = 275,
     FUNCTION = 276,
     ICONMGR_SHOW = 277,
     ICONMGR = 278,
     ALTER = 279,
     WINDOW_FUNCTION = 280,
     ZOOM = 281,
     ICONMGRS = 282,
     ICONMGR_GEOMETRY = 283,
     ICONMGR_NOSHOW = 284,
     MAKE_TITLE = 285,
     ICONIFY_BY_UNMAPPING = 286,
     DONT_ICONIFY_BY_UNMAPPING = 287,
     NO_BORDER = 288,
     NO_ICON_TITLE = 289,
     NO_TITLE = 290,
     AUTO_RAISE = 291,
     NO_HILITE = 292,
     ICON_REGION = 293,
     WINDOW_REGION = 294,
     META = 295,
     SHIFT = 296,
     LOCK = 297,
     CONTROL = 298,
     WINDOW = 299,
     TITLE = 300,
     ICON = 301,
     ROOT = 302,
     FRAME = 303,
     COLON = 304,
     EQUALS = 305,
     SQUEEZE_TITLE = 306,
     DONT_SQUEEZE_TITLE = 307,
     START_ICONIFIED = 308,
     NO_TITLE_HILITE = 309,
     TITLE_HILITE = 310,
     MOVE = 311,
     RESIZE = 312,
     WAITC = 313,
     SELECT = 314,
     KILL = 315,
     LEFT_TITLEBUTTON = 316,
     RIGHT_TITLEBUTTON = 317,
     NUMBER = 318,
     KEYWORD = 319,
     NKEYWORD = 320,
     CKEYWORD = 321,
     CLKEYWORD = 322,
     FKEYWORD = 323,
     FSKEYWORD = 324,
     SKEYWORD = 325,
     DKEYWORD = 326,
     JKEYWORD = 327,
     WINDOW_RING = 328,
     WINDOW_RING_EXCLUDE = 329,
     WARP_CURSOR = 330,
     ERRORTOKEN = 331,
     NO_STACKMODE = 332,
     ALWAYS_ON_TOP = 333,
     WORKSPACE = 334,
     WORKSPACES = 335,
     WORKSPCMGR_GEOMETRY = 336,
     OCCUPYALL = 337,
     OCCUPYLIST = 338,
     MAPWINDOWCURRENTWORKSPACE = 339,
     MAPWINDOWDEFAULTWORKSPACE = 340,
     UNMAPBYMOVINGFARAWAY = 341,
     OPAQUEMOVE = 342,
     NOOPAQUEMOVE = 343,
     OPAQUERESIZE = 344,
     NOOPAQUERESIZE = 345,
     DONTSETINACTIVE = 346,
     CHANGE_WORKSPACE_FUNCTION = 347,
     DEICONIFY_FUNCTION = 348,
     ICONIFY_FUNCTION = 349,
     AUTOSQUEEZE = 350,
     STARTSQUEEZED = 351,
     DONT_SAVE = 352,
     AUTO_LOWER = 353,
     ICONMENU_DONTSHOW = 354,
     WINDOW_BOX = 355,
     IGNOREMODIFIER = 356,
     WINDOW_GEOMETRIES = 357,
     ALWAYSSQUEEZETOGRAVITY = 358,
     VIRTUAL_SCREENS = 359,
     IGNORE_TRANSIENT = 360,
     DONTTOGGLEWORKSPACEMANAGERSTATE = 361,
     STRING = 362
   };
#endif
#define LB 258
#define RB 259
#define LP 260
#define RP 261
#define MENUS 262
#define MENU 263
#define BUTTON 264
#define DEFAULT_FUNCTION 265
#define PLUS 266
#define MINUS 267
#define ALL 268
#define OR 269
#define CURSORS 270
#define PIXMAPS 271
#define ICONS 272
#define COLOR 273
#define SAVECOLOR 274
#define MONOCHROME 275
#define FUNCTION 276
#define ICONMGR_SHOW 277
#define ICONMGR 278
#define ALTER 279
#define WINDOW_FUNCTION 280
#define ZOOM 281
#define ICONMGRS 282
#define ICONMGR_GEOMETRY 283
#define ICONMGR_NOSHOW 284
#define MAKE_TITLE 285
#define ICONIFY_BY_UNMAPPING 286
#define DONT_ICONIFY_BY_UNMAPPING 287
#define NO_BORDER 288
#define NO_ICON_TITLE 289
#define NO_TITLE 290
#define AUTO_RAISE 291
#define NO_HILITE 292
#define ICON_REGION 293
#define WINDOW_REGION 294
#define META 295
#define SHIFT 296
#define LOCK 297
#define CONTROL 298
#define WINDOW 299
#define TITLE 300
#define ICON 301
#define ROOT 302
#define FRAME 303
#define COLON 304
#define EQUALS 305
#define SQUEEZE_TITLE 306
#define DONT_SQUEEZE_TITLE 307
#define START_ICONIFIED 308
#define NO_TITLE_HILITE 309
#define TITLE_HILITE 310
#define MOVE 311
#define RESIZE 312
#define WAITC 313
#define SELECT 314
#define KILL 315
#define LEFT_TITLEBUTTON 316
#define RIGHT_TITLEBUTTON 317
#define NUMBER 318
#define KEYWORD 319
#define NKEYWORD 320
#define CKEYWORD 321
#define CLKEYWORD 322
#define FKEYWORD 323
#define FSKEYWORD 324
#define SKEYWORD 325
#define DKEYWORD 326
#define JKEYWORD 327
#define WINDOW_RING 328
#define WINDOW_RING_EXCLUDE 329
#define WARP_CURSOR 330
#define ERRORTOKEN 331
#define NO_STACKMODE 332
#define ALWAYS_ON_TOP 333
#define WORKSPACE 334
#define WORKSPACES 335
#define WORKSPCMGR_GEOMETRY 336
#define OCCUPYALL 337
#define OCCUPYLIST 338
#define MAPWINDOWCURRENTWORKSPACE 339
#define MAPWINDOWDEFAULTWORKSPACE 340
#define UNMAPBYMOVINGFARAWAY 341
#define OPAQUEMOVE 342
#define NOOPAQUEMOVE 343
#define OPAQUERESIZE 344
#define NOOPAQUERESIZE 345
#define DONTSETINACTIVE 346
#define CHANGE_WORKSPACE_FUNCTION 347
#define DEICONIFY_FUNCTION 348
#define ICONIFY_FUNCTION 349
#define AUTOSQUEEZE 350
#define STARTSQUEEZED 351
#define DONT_SAVE 352
#define AUTO_LOWER 353
#define ICONMENU_DONTSHOW 354
#define WINDOW_BOX 355
#define IGNOREMODIFIER 356
#define WINDOW_GEOMETRIES 357
#define ALWAYSSQUEEZETOGRAVITY 358
#define VIRTUAL_SCREENS 359
#define IGNORE_TRANSIENT 360
#define DONTTOGGLEWORKSPACEMANAGERSTATE 361
#define STRING 362




/* Copy the first part of user declarations.  */
#line 67 "gram.y"

#include <stdio.h>
#include <ctype.h>
#include "twm.h"
#include "menus.h"
#include "icons.h"
#include "windowbox.h"
#include "add_window.h"
#include "list.h"
#include "util.h"
#include "screen.h"
#include "parse.h"
#include "cursor.h"
#ifdef VMS
#  include <decw$include/Xos.h>
#  include <X11Xmu/CharSet.h>
#else
#  include <X11/Xos.h>
#  include <X11/Xmu/CharSet.h>
#endif

static char *Action = "";
static char *Name = "";
static char *defstring = "default";
static MenuRoot	*root, *pull = NULL;
static char *curWorkSpc;
static char *client, *workspace;
static MenuItem *lastmenuitem = (MenuItem*) 0;

extern void yyerror(char *s);
extern void RemoveDQuote(char *str);

static MenuRoot *GetRoot(char *name, char *fore, char *back);

static Bool CheckWarpScreenArg(register char *s);
static Bool CheckWarpRingArg(register char *s);
static Bool CheckColormapArg(register char *s);
static void GotButton(int butt, int func);
static void GotKey(char *key, int func);
static void GotTitleButton(char *bitmapname, int func, Bool rightside);
static char *ptr;
static name_list **list;
static int cont = 0;
static int color;
Bool donttoggleworkspacemanagerstate = FALSE;
int mods = 0;
unsigned int mods_used = (ShiftMask | ControlMask | Mod1Mask);

extern void twmrc_error_prefix(void);

extern int yylex(void);


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 121 "gram.y"
typedef union YYSTYPE {
    int num;
    char *ptr;
} YYSTYPE;
/* Line 191 of yacc.c.  */
#line 348 "y.tab.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */
#line 360 "y.tab.c"

#if ! defined (yyoverflow) || YYERROR_VERBOSE

# ifndef YYFREE
#  define YYFREE free
# endif
# ifndef YYMALLOC
#  define YYMALLOC malloc
# endif

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   define YYSTACK_ALLOC alloca
#  endif
# else
#  if defined (alloca) || defined (_ALLOCA_H)
#   define YYSTACK_ALLOC alloca
#  else
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   842

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  108
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  140
/* YYNRULES -- Number of rules. */
#define YYNRULES  334
/* YYNRULES -- Number of states. */
#define YYNSTATES  480

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   362

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     6,     9,    11,    13,    15,    17,
      19,    26,    34,    43,    53,    54,    63,    64,    74,    75,
      86,    87,    99,   100,   107,   108,   114,   118,   121,   125,
     128,   129,   133,   134,   138,   141,   143,   146,   149,   150,
     154,   156,   157,   161,   163,   164,   168,   170,   171,   175,
     177,   178,   182,   184,   189,   194,   195,   200,   201,   206,
     209,   212,   215,   218,   219,   223,   224,   228,   229,   230,
     237,   238,   242,   243,   247,   248,   252,   253,   257,   258,
     262,   263,   267,   269,   270,   274,   275,   279,   280,   284,
     286,   287,   291,   292,   296,   297,   301,   303,   304,   308,
     310,   311,   315,   316,   320,   322,   323,   327,   328,   332,
     333,   337,   339,   340,   344,   346,   347,   351,   353,   354,
     358,   359,   363,   364,   368,   370,   371,   375,   377,   378,
     388,   389,   394,   395,   400,   401,   405,   406,   410,   411,
     415,   416,   420,   423,   426,   429,   432,   435,   436,   440,
     442,   443,   447,   449,   450,   454,   455,   459,   460,   464,
     466,   469,   471,   474,   481,   488,   489,   492,   494,   496,
     498,   500,   503,   506,   508,   509,   512,   514,   516,   518,
     520,   522,   524,   526,   528,   530,   532,   534,   535,   538,
     540,   542,   544,   546,   548,   550,   552,   554,   556,   558,
     560,   562,   566,   567,   570,   574,   578,   579,   582,   585,
     589,   590,   593,   597,   600,   604,   607,   611,   614,   618,
     621,   625,   628,   632,   635,   639,   642,   646,   649,   653,
     656,   660,   663,   667,   670,   674,   675,   678,   681,   682,
     687,   690,   694,   695,   698,   700,   702,   706,   707,   710,
     713,   717,   718,   721,   724,   728,   729,   732,   734,   736,
     737,   743,   745,   746,   750,   751,   757,   761,   762,   765,
     769,   774,   778,   779,   782,   784,   785,   789,   793,   794,
     797,   799,   802,   806,   811,   817,   821,   826,   832,   839,
     843,   848,   854,   861,   865,   866,   869,   871,   875,   876,
     879,   880,   884,   885,   890,   891,   896,   900,   901,   904,
     906,   910,   911,   914,   916,   920,   921,   924,   927,   931,
     932,   935,   937,   941,   942,   945,   948,   956,   958,   961,
     963,   966,   969,   972,   974
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short int yyrhs[] =
{
     109,     0,    -1,   110,    -1,    -1,   110,   111,    -1,     1,
      -1,   167,    -1,   168,    -1,   169,    -1,   203,    -1,    38,
     246,    71,    71,   247,   247,    -1,    38,   246,    71,    71,
     247,   247,   246,    -1,    38,   246,    71,    71,   247,   247,
     246,   246,    -1,    38,   246,    71,    71,   247,   247,   246,
     246,   246,    -1,    -1,    38,   246,    71,    71,   247,   247,
     112,   219,    -1,    -1,    38,   246,    71,    71,   247,   247,
     246,   113,   219,    -1,    -1,    38,   246,    71,    71,   247,
     247,   246,   246,   114,   219,    -1,    -1,    38,   246,    71,
      71,   247,   247,   246,   246,   246,   115,   219,    -1,    -1,
      39,   246,    71,    71,   116,   219,    -1,    -1,   100,   246,
     246,   117,   219,    -1,    28,   246,   247,    -1,    28,   246,
      -1,    81,   246,   247,    -1,    81,   246,    -1,    -1,    84,
     118,   217,    -1,    -1,    85,   119,   218,    -1,    26,   247,
      -1,    26,    -1,    16,   181,    -1,    15,   184,    -1,    -1,
      31,   120,   219,    -1,    31,    -1,    -1,    87,   121,   219,
      -1,    87,    -1,    -1,    88,   122,   219,    -1,    88,    -1,
      -1,    89,   123,   219,    -1,    89,    -1,    -1,    90,   124,
     219,    -1,    90,    -1,    61,   246,    50,   243,    -1,    62,
     246,    50,   243,    -1,    -1,    61,   246,   125,   178,    -1,
      -1,    62,   246,   126,   178,    -1,   245,   246,    -1,   245,
     243,    -1,   246,   171,    -1,   245,   170,    -1,    -1,    32,
     127,   219,    -1,    -1,    80,   128,   210,    -1,    -1,    -1,
     101,   129,     3,   172,   130,     4,    -1,    -1,    82,   131,
     219,    -1,    -1,    99,   132,   219,    -1,    -1,    83,   133,
     222,    -1,    -1,    86,   134,   219,    -1,    -1,    95,   135,
     219,    -1,    -1,    96,   136,   219,    -1,   103,    -1,    -1,
     103,   137,   219,    -1,    -1,    91,   138,   219,    -1,    -1,
      29,   139,   219,    -1,    29,    -1,    -1,    27,   140,   207,
      -1,    -1,    22,   141,   219,    -1,    -1,    54,   142,   219,
      -1,    54,    -1,    -1,    37,   143,   219,    -1,    37,    -1,
      -1,    78,   144,   219,    -1,    -1,    77,   145,   219,    -1,
      77,    -1,    -1,    33,   146,   219,    -1,    -1,    97,   147,
     219,    -1,    -1,    34,   148,   219,    -1,    34,    -1,    -1,
      35,   149,   219,    -1,    35,    -1,    -1,   105,   150,   219,
      -1,   106,    -1,    -1,    30,   151,   219,    -1,    -1,    53,
     152,   219,    -1,    -1,    36,   153,   219,    -1,    36,    -1,
      -1,    98,   154,   219,    -1,    98,    -1,    -1,     8,   246,
       5,   246,    49,   246,     6,   155,   240,    -1,    -1,     8,
     246,   156,   240,    -1,    -1,    21,   246,   157,   237,    -1,
      -1,    17,   158,   234,    -1,    -1,    18,   159,   187,    -1,
      -1,    19,   160,   191,    -1,    -1,    20,   161,   187,    -1,
      10,   243,    -1,    25,   243,    -1,    92,   243,    -1,    93,
     243,    -1,    94,   243,    -1,    -1,    75,   162,   219,    -1,
      75,    -1,    -1,    73,   163,   219,    -1,    73,    -1,    -1,
      74,   164,   219,    -1,    -1,   102,   165,   197,    -1,    -1,
     104,   166,   200,    -1,    64,    -1,    70,   246,    -1,    70,
      -1,    65,   247,    -1,    50,   172,    49,   174,    49,   243,
      -1,    50,   172,    49,   176,    49,   243,    -1,    -1,   172,
     173,    -1,    40,    -1,    41,    -1,    42,    -1,    43,    -1,
      24,   247,    -1,    40,   247,    -1,    14,    -1,    -1,   174,
     175,    -1,    44,    -1,    45,    -1,    46,    -1,    47,    -1,
      48,    -1,    79,    -1,    23,    -1,    40,    -1,    24,    -1,
      13,    -1,    14,    -1,    -1,   176,   177,    -1,    44,    -1,
      45,    -1,    46,    -1,    47,    -1,    48,    -1,    79,    -1,
      23,    -1,    40,    -1,    13,    -1,    24,    -1,    14,    -1,
     246,    -1,     3,   179,     4,    -1,    -1,   179,   180,    -1,
     245,    49,   243,    -1,     3,   182,     4,    -1,    -1,   182,
     183,    -1,    55,   246,    -1,     3,   185,     4,    -1,    -1,
     185,   186,    -1,    48,   246,   246,    -1,    48,   246,    -1,
      45,   246,   246,    -1,    45,   246,    -1,    46,   246,   246,
      -1,    46,   246,    -1,    23,   246,   246,    -1,    23,   246,
      -1,     9,   246,   246,    -1,     9,   246,    -1,    56,   246,
     246,    -1,    56,   246,    -1,    57,   246,   246,    -1,    57,
     246,    -1,    58,   246,   246,    -1,    58,   246,    -1,     8,
     246,   246,    -1,     8,   246,    -1,    59,   246,   246,    -1,
      59,   246,    -1,    60,   246,   246,    -1,    60,   246,    -1,
       3,   188,     4,    -1,    -1,   188,   189,    -1,    67,   246,
      -1,    -1,    67,   246,   190,   194,    -1,    66,   246,    -1,
       3,   192,     4,    -1,    -1,   192,   193,    -1,   246,    -1,
      67,    -1,     3,   195,     4,    -1,    -1,   195,   196,    -1,
     246,   246,    -1,     3,   198,     4,    -1,    -1,   198,   199,
      -1,   246,   246,    -1,     3,   201,     4,    -1,    -1,   201,
     202,    -1,   246,    -1,    51,    -1,    -1,    51,   204,     3,
     206,     4,    -1,    52,    -1,    -1,    52,   205,   219,    -1,
      -1,   206,   246,    72,   244,   247,    -1,     3,   208,     4,
      -1,    -1,   208,   209,    -1,   246,   246,   247,    -1,   246,
     246,   246,   247,    -1,     3,   211,     4,    -1,    -1,   211,
     212,    -1,   246,    -1,    -1,   246,   213,   214,    -1,     3,
     215,     4,    -1,    -1,   215,   216,    -1,   246,    -1,   246,
     246,    -1,   246,   246,   246,    -1,   246,   246,   246,   246,
      -1,   246,   246,   246,   246,   246,    -1,     3,   246,     4,
      -1,     3,   246,   246,     4,    -1,     3,   246,   246,   246,
       4,    -1,     3,   246,   246,   246,   246,     4,    -1,     3,
     246,     4,    -1,     3,   246,   246,     4,    -1,     3,   246,
     246,   246,     4,    -1,     3,   246,   246,   246,   246,     4,
      -1,     3,   220,     4,    -1,    -1,   220,   221,    -1,   246,
      -1,     3,   223,     4,    -1,    -1,   223,   224,    -1,    -1,
     246,   225,   228,    -1,    -1,    44,   246,   226,   228,    -1,
      -1,    79,   246,   227,   231,    -1,     3,   229,     4,    -1,
      -1,   229,   230,    -1,   246,    -1,     3,   232,     4,    -1,
      -1,   232,   233,    -1,   246,    -1,     3,   235,     4,    -1,
      -1,   235,   236,    -1,   246,   246,    -1,     3,   238,     4,
      -1,    -1,   238,   239,    -1,   243,    -1,     3,   241,     4,
      -1,    -1,   241,   242,    -1,   246,   243,    -1,   246,     5,
     246,    49,   246,     6,   243,    -1,    68,    -1,    69,   246,
      -1,   247,    -1,    11,   247,    -1,    12,   247,    -1,     9,
     247,    -1,   107,    -1,    63,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   154,   154,   157,   158,   161,   162,   163,   164,   165,
     166,   169,   172,   175,   178,   178,   182,   182,   186,   186,
     190,   190,   195,   195,   200,   200,   205,   211,   214,   220,
     223,   223,   226,   226,   229,   235,   237,   238,   239,   239,
     241,   244,   244,   246,   247,   247,   249,   250,   250,   252,
     253,   253,   255,   257,   260,   263,   263,   265,   265,   267,
     271,   287,   288,   290,   290,   292,   292,   294,   294,   294,
     295,   295,   297,   297,   299,   299,   301,   301,   303,   303,
     305,   305,   307,   308,   308,   310,   310,   312,   312,   314,
     315,   315,   317,   317,   319,   319,   321,   323,   323,   325,
     327,   327,   329,   329,   331,   333,   333,   335,   335,   337,
     337,   339,   341,   341,   343,   345,   345,   347,   348,   348,
     350,   350,   352,   352,   354,   355,   355,   357,   358,   358,
     361,   361,   363,   363,   365,   365,   367,   367,   369,   369,
     371,   371,   373,   389,   397,   405,   413,   421,   421,   423,
     425,   425,   427,   428,   428,   432,   432,   434,   434,   437,
     447,   455,   465,   477,   480,   483,   484,   487,   488,   489,
     490,   491,   501,   511,   514,   515,   518,   519,   520,   521,
     522,   523,   524,   525,   526,   527,   528,   531,   532,   535,
     536,   537,   538,   539,   540,   541,   542,   543,   544,   545,
     546,   550,   553,   554,   557,   561,   564,   565,   568,   572,
     575,   576,   579,   581,   583,   585,   587,   589,   591,   593,
     595,   597,   599,   601,   603,   605,   607,   609,   611,   613,
     615,   617,   619,   621,   625,   629,   630,   633,   642,   642,
     653,   664,   667,   668,   671,   672,   675,   678,   679,   682,
     687,   690,   691,   694,   697,   700,   701,   704,   707,   710,
     710,   715,   716,   716,   720,   721,   729,   732,   733,   736,
     741,   749,   752,   753,   756,   759,   759,   765,   768,   769,
     772,   775,   778,   781,   784,   789,   792,   795,   798,   803,
     806,   809,   812,   817,   820,   821,   824,   829,   832,   833,
     836,   836,   838,   838,   840,   840,   844,   847,   848,   851,
     856,   859,   860,   863,   867,   870,   871,   874,   877,   880,
     881,   884,   890,   893,   894,   897,   907,   919,   920,   961,
     962,   963,   966,   978,   985
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "LB", "RB", "LP", "RP", "MENUS", "MENU",
  "BUTTON", "DEFAULT_FUNCTION", "PLUS", "MINUS", "ALL", "OR", "CURSORS",
  "PIXMAPS", "ICONS", "COLOR", "SAVECOLOR", "MONOCHROME", "FUNCTION",
  "ICONMGR_SHOW", "ICONMGR", "ALTER", "WINDOW_FUNCTION", "ZOOM",
  "ICONMGRS", "ICONMGR_GEOMETRY", "ICONMGR_NOSHOW", "MAKE_TITLE",
  "ICONIFY_BY_UNMAPPING", "DONT_ICONIFY_BY_UNMAPPING", "NO_BORDER",
  "NO_ICON_TITLE", "NO_TITLE", "AUTO_RAISE", "NO_HILITE", "ICON_REGION",
  "WINDOW_REGION", "META", "SHIFT", "LOCK", "CONTROL", "WINDOW", "TITLE",
  "ICON", "ROOT", "FRAME", "COLON", "EQUALS", "SQUEEZE_TITLE",
  "DONT_SQUEEZE_TITLE", "START_ICONIFIED", "NO_TITLE_HILITE",
  "TITLE_HILITE", "MOVE", "RESIZE", "WAITC", "SELECT", "KILL",
  "LEFT_TITLEBUTTON", "RIGHT_TITLEBUTTON", "NUMBER", "KEYWORD", "NKEYWORD",
  "CKEYWORD", "CLKEYWORD", "FKEYWORD", "FSKEYWORD", "SKEYWORD", "DKEYWORD",
  "JKEYWORD", "WINDOW_RING", "WINDOW_RING_EXCLUDE", "WARP_CURSOR",
  "ERRORTOKEN", "NO_STACKMODE", "ALWAYS_ON_TOP", "WORKSPACE", "WORKSPACES",
  "WORKSPCMGR_GEOMETRY", "OCCUPYALL", "OCCUPYLIST",
  "MAPWINDOWCURRENTWORKSPACE", "MAPWINDOWDEFAULTWORKSPACE",
  "UNMAPBYMOVINGFARAWAY", "OPAQUEMOVE", "NOOPAQUEMOVE", "OPAQUERESIZE",
  "NOOPAQUERESIZE", "DONTSETINACTIVE", "CHANGE_WORKSPACE_FUNCTION",
  "DEICONIFY_FUNCTION", "ICONIFY_FUNCTION", "AUTOSQUEEZE", "STARTSQUEEZED",
  "DONT_SAVE", "AUTO_LOWER", "ICONMENU_DONTSHOW", "WINDOW_BOX",
  "IGNOREMODIFIER", "WINDOW_GEOMETRIES", "ALWAYSSQUEEZETOGRAVITY",
  "VIRTUAL_SCREENS", "IGNORE_TRANSIENT", "DONTTOGGLEWORKSPACEMANAGERSTATE",
  "STRING", "$accept", "twmrc", "stmts", "stmt", "@1", "@2", "@3", "@4",
  "@5", "@6", "@7", "@8", "@9", "@10", "@11", "@12", "@13", "@14", "@15",
  "@16", "@17", "@18", "@19", "@20", "@21", "@22", "@23", "@24", "@25",
  "@26", "@27", "@28", "@29", "@30", "@31", "@32", "@33", "@34", "@35",
  "@36", "@37", "@38", "@39", "@40", "@41", "@42", "@43", "@44", "@45",
  "@46", "@47", "@48", "@49", "@50", "@51", "@52", "@53", "@54", "@55",
  "noarg", "sarg", "narg", "full", "fullkey", "keys", "key", "contexts",
  "context", "contextkeys", "contextkey", "binding_list",
  "binding_entries", "binding_entry", "pixmap_list", "pixmap_entries",
  "pixmap_entry", "cursor_list", "cursor_entries", "cursor_entry",
  "color_list", "color_entries", "color_entry", "@56", "save_color_list",
  "s_color_entries", "s_color_entry", "win_color_list",
  "win_color_entries", "win_color_entry", "wingeom_list",
  "wingeom_entries", "wingeom_entry", "geom_list", "geom_entries",
  "geom_entry", "squeeze", "@57", "@58", "win_sqz_entries", "iconm_list",
  "iconm_entries", "iconm_entry", "workspc_list", "workspc_entries",
  "workspc_entry", "@59", "workapp_list", "workapp_entries",
  "workapp_entry", "curwork", "defwork", "win_list", "win_entries",
  "win_entry", "occupy_list", "occupy_entries", "occupy_entry", "@60",
  "@61", "@62", "occupy_workspc_list", "occupy_workspc_entries",
  "occupy_workspc_entry", "occupy_window_list", "occupy_window_entries",
  "occupy_window_entry", "icon_list", "icon_entries", "icon_entry",
  "function", "function_entries", "function_entry", "menu", "menu_entries",
  "menu_entry", "action", "signed_number", "button", "string", "number", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,   108,   109,   110,   110,   111,   111,   111,   111,   111,
     111,   111,   111,   111,   112,   111,   113,   111,   114,   111,
     115,   111,   116,   111,   117,   111,   111,   111,   111,   111,
     118,   111,   119,   111,   111,   111,   111,   111,   120,   111,
     111,   121,   111,   111,   122,   111,   111,   123,   111,   111,
     124,   111,   111,   111,   111,   125,   111,   126,   111,   111,
     111,   111,   111,   127,   111,   128,   111,   129,   130,   111,
     131,   111,   132,   111,   133,   111,   134,   111,   135,   111,
     136,   111,   111,   137,   111,   138,   111,   139,   111,   111,
     140,   111,   141,   111,   142,   111,   111,   143,   111,   111,
     144,   111,   145,   111,   111,   146,   111,   147,   111,   148,
     111,   111,   149,   111,   111,   150,   111,   111,   151,   111,
     152,   111,   153,   111,   111,   154,   111,   111,   155,   111,
     156,   111,   157,   111,   158,   111,   159,   111,   160,   111,
     161,   111,   111,   111,   111,   111,   111,   162,   111,   111,
     163,   111,   111,   164,   111,   165,   111,   166,   111,   167,
     168,   168,   169,   170,   171,   172,   172,   173,   173,   173,
     173,   173,   173,   173,   174,   174,   175,   175,   175,   175,
     175,   175,   175,   175,   175,   175,   175,   176,   176,   177,
     177,   177,   177,   177,   177,   177,   177,   177,   177,   177,
     177,   178,   179,   179,   180,   181,   182,   182,   183,   184,
     185,   185,   186,   186,   186,   186,   186,   186,   186,   186,
     186,   186,   186,   186,   186,   186,   186,   186,   186,   186,
     186,   186,   186,   186,   187,   188,   188,   189,   190,   189,
     189,   191,   192,   192,   193,   193,   194,   195,   195,   196,
     197,   198,   198,   199,   200,   201,   201,   202,   203,   204,
     203,   203,   205,   203,   206,   206,   207,   208,   208,   209,
     209,   210,   211,   211,   212,   213,   212,   214,   215,   215,
     216,   216,   216,   216,   216,   217,   217,   217,   217,   218,
     218,   218,   218,   219,   220,   220,   221,   222,   223,   223,
     225,   224,   226,   224,   227,   224,   228,   229,   229,   230,
     231,   232,   232,   233,   234,   235,   235,   236,   237,   238,
     238,   239,   240,   241,   241,   242,   242,   243,   243,   244,
     244,   244,   245,   246,   247
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     0,     2,     1,     1,     1,     1,     1,
       6,     7,     8,     9,     0,     8,     0,     9,     0,    10,
       0,    11,     0,     6,     0,     5,     3,     2,     3,     2,
       0,     3,     0,     3,     2,     1,     2,     2,     0,     3,
       1,     0,     3,     1,     0,     3,     1,     0,     3,     1,
       0,     3,     1,     4,     4,     0,     4,     0,     4,     2,
       2,     2,     2,     0,     3,     0,     3,     0,     0,     6,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     1,     0,     3,     0,     3,     0,     3,     1,
       0,     3,     0,     3,     0,     3,     1,     0,     3,     1,
       0,     3,     0,     3,     1,     0,     3,     0,     3,     0,
       3,     1,     0,     3,     1,     0,     3,     1,     0,     3,
       0,     3,     0,     3,     1,     0,     3,     1,     0,     9,
       0,     4,     0,     4,     0,     3,     0,     3,     0,     3,
       0,     3,     2,     2,     2,     2,     2,     0,     3,     1,
       0,     3,     1,     0,     3,     0,     3,     0,     3,     1,
       2,     1,     2,     6,     6,     0,     2,     1,     1,     1,
       1,     2,     2,     1,     0,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     0,     2,     3,     3,     0,     2,     2,     3,
       0,     2,     3,     2,     3,     2,     3,     2,     3,     2,
       3,     2,     3,     2,     3,     2,     3,     2,     3,     2,
       3,     2,     3,     2,     3,     0,     2,     2,     0,     4,
       2,     3,     0,     2,     1,     1,     3,     0,     2,     2,
       3,     0,     2,     2,     3,     0,     2,     1,     1,     0,
       5,     1,     0,     3,     0,     5,     3,     0,     2,     3,
       4,     3,     0,     2,     1,     0,     3,     3,     0,     2,
       1,     2,     3,     4,     5,     3,     4,     5,     6,     3,
       4,     5,     6,     3,     0,     2,     1,     3,     0,     2,
       0,     3,     0,     4,     0,     4,     3,     0,     2,     1,
       3,     0,     2,     1,     3,     0,     2,     2,     3,     0,
       2,     1,     3,     0,     2,     2,     7,     1,     2,     1,
       2,     2,     2,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short int yydefact[] =
{
       3,     0,     0,     1,     5,     0,     0,     0,     0,     0,
     134,   136,   138,   140,     0,    92,     0,    35,    90,     0,
      89,   118,    40,    63,   105,   111,   114,   124,    99,     0,
       0,   258,   261,   120,    96,     0,     0,   159,     0,   161,
     152,   153,   149,   104,   100,    65,     0,    70,    74,    30,
      32,    76,    43,    46,    49,    52,    85,     0,     0,     0,
      78,    80,   107,   127,    72,     0,    67,   155,    82,   157,
     115,   117,   333,     4,     6,     7,     8,     9,     0,     0,
     130,   334,   332,   327,     0,   142,   210,    37,   206,    36,
       0,     0,     0,     0,   132,     0,   143,    34,     0,    27,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    55,    57,   162,   160,     0,
       0,     0,     0,     0,     0,    29,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   144,   145,   146,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     165,    62,    60,    59,   165,    61,     0,     0,   328,     0,
       0,   315,   135,   235,   137,   242,   139,   141,     0,   294,
      93,   267,    91,    26,    88,   119,    39,    64,   106,   110,
     113,   123,    98,     0,     0,   264,   263,   121,    95,     0,
       0,     0,     0,   151,   154,   148,   103,   101,   272,    66,
      28,    71,   298,    75,     0,    31,     0,    33,    77,    42,
      45,    48,    51,    86,    79,    81,   108,   126,    73,    24,
     165,   251,   156,    84,   255,   158,   116,     0,     0,     0,
     323,   131,   209,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   211,   205,     0,   207,     0,     0,
       0,   319,   133,     0,     0,     0,    22,     0,    53,   202,
      56,    54,    58,     0,     0,     0,     0,     0,    68,     0,
       0,   173,     0,   167,   168,   169,   170,   174,   166,   187,
       0,     0,   229,   221,   219,   215,   217,   213,   223,   225,
     227,   231,   233,   208,   314,   316,     0,   234,     0,     0,
     236,   241,   245,   243,   244,     0,   293,   295,   296,   266,
     268,     0,     0,     0,   260,     0,     0,   271,   273,   274,
     297,     0,     0,   299,   300,   285,     0,   289,     0,    25,
       0,   250,   252,     0,   254,   256,   257,   171,   172,     0,
       0,     0,   322,   324,     0,   228,   220,   218,   214,   216,
     212,   222,   224,   226,   230,   232,   317,   240,   237,   318,
     320,   321,     0,    10,    23,     0,   201,   203,     0,     0,
     302,   304,     0,   286,     0,   290,     0,    69,   253,   185,
     186,   182,   184,   183,   176,   177,   178,   179,   180,     0,
     181,   175,   197,   199,   195,   198,   196,   189,   190,   191,
     192,   193,     0,   194,   188,   200,   128,     0,   325,     0,
       0,   269,     0,    11,     0,     0,     0,   329,     0,   278,
     276,     0,     0,   307,   301,   287,     0,   291,     0,   163,
     164,     0,     0,   247,   239,   270,    15,     0,    12,   330,
     331,   265,   204,     0,   303,   311,   305,     0,   288,   292,
     129,     0,     0,    17,     0,    13,   277,   279,   280,     0,
     306,   308,   309,     0,   246,   248,     0,    19,     0,   281,
     310,   312,   313,     0,   249,    21,   282,   326,   283,   284
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,     1,     2,    73,   412,   437,   454,   468,   313,   267,
     128,   129,   102,   131,   132,   133,   134,   190,   192,   103,
     124,   145,   330,   126,   143,   127,   130,   139,   140,   147,
     135,   100,    98,    95,   114,   108,   123,   122,   104,   141,
     105,   106,   149,   101,   113,   107,   142,   431,   157,   168,
      90,    91,    92,    93,   121,   119,   120,   146,   148,    74,
      75,    76,   151,   155,   227,   278,   339,   391,   340,   404,
     260,   316,   367,    89,   160,   247,    87,   159,   244,   164,
     249,   300,   409,   166,   250,   303,   434,   452,   465,   222,
     269,   332,   225,   270,   335,    77,   111,   112,   257,   172,
     254,   310,   199,   263,   318,   369,   420,   443,   457,   205,
     207,   170,   253,   307,   203,   264,   323,   372,   421,   422,
     424,   447,   461,   446,   459,   471,   162,   248,   295,   252,
     305,   360,   231,   281,   343,    85,   416,    78,    79,    82
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -207
static const short int yypact[] =
{
    -207,    39,   510,  -207,  -207,   -71,   -18,   -16,    46,    53,
    -207,  -207,  -207,  -207,   -71,  -207,   -16,   -18,  -207,   -71,
      54,  -207,    56,  -207,  -207,    61,    63,    64,    65,   -71,
     -71,    66,    67,  -207,    77,   -71,   -71,  -207,   -18,   -71,
      78,  -207,    81,    83,  -207,  -207,   -71,  -207,  -207,  -207,
    -207,  -207,    86,    87,   109,   114,  -207,   -16,   -16,   -16,
    -207,  -207,  -207,   116,  -207,   -71,  -207,  -207,   125,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,    35,    84,
     128,  -207,  -207,  -207,   -71,  -207,  -207,  -207,  -207,  -207,
     140,   141,   142,   141,  -207,   146,  -207,  -207,   147,   -18,
     146,   146,   146,   146,   146,   146,   146,   146,   146,    75,
      82,   151,   146,   146,   146,   108,   113,  -207,  -207,   146,
     146,   146,   146,   146,   156,   -18,   146,   162,   163,   174,
     146,   146,   146,   146,   146,   146,  -207,  -207,  -207,   146,
     146,   146,   146,   146,   -71,   179,   185,   146,   186,   146,
    -207,  -207,  -207,  -207,  -207,  -207,   -71,   187,  -207,   152,
      36,  -207,  -207,  -207,  -207,  -207,  -207,  -207,   188,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,   121,   122,  -207,  -207,  -207,  -207,   -16,
     191,   -16,   191,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,   -71,  -207,   -71,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,    34,   138,   153,
    -207,  -207,  -207,   -71,   -71,   -71,   -71,   -71,   -71,   -71,
     -71,   -71,   -71,   -71,  -207,  -207,   -71,  -207,     4,    74,
      -2,  -207,  -207,     6,     7,   -18,  -207,     8,  -207,  -207,
    -207,  -207,  -207,     9,     3,    11,    13,   146,   143,    14,
      15,  -207,   -18,   -18,  -207,  -207,  -207,  -207,  -207,  -207,
     -71,    16,   -71,   -71,   -71,   -71,   -71,   -71,   -71,   -71,
     -71,   -71,   -71,  -207,  -207,  -207,   -71,  -207,   -71,   -71,
    -207,  -207,  -207,  -207,  -207,    31,  -207,  -207,  -207,  -207,
    -207,   -71,   -18,   146,  -207,   123,    42,  -207,  -207,   193,
    -207,   -71,   -71,  -207,  -207,  -207,    18,  -207,    19,  -207,
     200,  -207,  -207,   -71,  -207,  -207,  -207,  -207,  -207,   124,
      48,   199,  -207,  -207,    33,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,   203,  -207,
    -207,  -207,   -57,     0,  -207,    43,  -207,  -207,   158,   210,
    -207,  -207,   211,  -207,    22,  -207,    23,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,   -16,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,   -16,  -207,  -207,  -207,  -207,   -71,  -207,   212,
     -18,  -207,   146,     1,   -18,   -18,   -18,  -207,   -16,  -207,
    -207,   211,   213,  -207,  -207,  -207,   214,  -207,   215,  -207,
    -207,   187,   168,  -207,  -207,  -207,  -207,   146,     2,  -207,
    -207,  -207,  -207,    24,  -207,  -207,  -207,    25,  -207,  -207,
    -207,   -71,    28,  -207,   146,   217,  -207,  -207,   -71,    29,
    -207,  -207,  -207,   216,  -207,  -207,   -71,  -207,   146,   -71,
    -207,  -207,  -207,   -16,  -207,  -207,   -71,  -207,   -71,  -207
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -133,  -207,  -207,  -207,  -207,  -207,
      32,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,   130,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,   374,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -200,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,  -207,
    -207,  -207,  -206,  -207,  -207,   -15,  -207,   -90,    -5,    -1
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -276
static const short int yytable[] =
{
      80,    96,   301,   -14,   -16,   -18,    81,   320,   294,    94,
     306,   309,   314,   317,    99,   325,    97,   327,   331,   334,
     342,   228,   373,   375,   109,   110,   425,   427,   456,   460,
     115,   116,   464,   470,   118,   359,    72,   117,   407,     3,
     245,   125,   136,   137,   138,    81,   366,   321,   271,    86,
      72,     6,    83,    84,   414,   415,    88,   -87,   272,   -38,
     144,   392,   393,   152,  -109,   302,  -112,  -122,   -97,  -259,
    -262,   394,   395,   153,   273,   274,   275,   276,   297,   158,
     -94,  -150,   322,   277,  -147,   150,  -102,   268,   396,   -41,
     -44,   246,   397,   398,   399,   400,   401,   402,   173,    83,
      84,    83,    84,    83,    84,    72,    81,    72,    72,    72,
      72,    72,   -47,    72,    72,    72,    72,   -50,    72,  -125,
      72,    72,    72,    72,   200,    72,    72,   403,   -83,    72,
      72,    72,    72,   156,   154,    72,    72,   379,   380,   219,
     298,   299,    72,   161,   163,   165,   183,   381,   382,   169,
     171,   229,   271,   184,   185,    72,   232,   271,   189,   198,
     233,   234,   272,   191,   383,   202,   204,   272,   384,   385,
     386,   387,   388,   389,   258,   235,   261,   206,   273,   274,
     275,   276,   220,   273,   274,   275,   276,   279,   221,   224,
     230,   251,   255,   256,   259,   365,  -275,   236,   237,   265,
     238,   266,   280,   390,   377,   406,  -238,   418,   239,   240,
     241,   242,   243,   419,   423,   433,   445,   451,   448,   449,
     -20,   444,   473,   167,   262,   450,   368,     0,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,     0,
       0,   293,     0,   296,     0,   304,     0,     0,   308,   311,
       0,     0,   315,     0,   312,     0,     0,     0,   319,   324,
     326,   328,     0,     0,   333,   336,     0,     0,     0,     0,
       0,   337,   338,     0,     0,   341,   344,   345,   346,   347,
     348,   349,   350,   351,   352,   353,   354,   355,     0,     0,
     361,   356,     0,   357,   358,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   362,     0,     0,     0,
       0,   363,     0,     0,     0,     0,   370,   371,     0,     0,
       0,   374,     0,   376,     0,     0,     0,     0,   378,   408,
       0,     0,     0,     0,     0,   405,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   410,   413,     0,
       0,   411,     0,     0,   417,     0,     0,     0,     0,   426,
       0,   428,     0,     0,   429,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   430,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   432,   442,     0,     0,     0,     0,   438,   435,
       0,     0,     0,   439,   440,   441,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   455,     0,     0,     0,     0,   458,     0,
       0,     0,   462,     0,     0,     0,   463,   466,     0,     0,
       0,     0,     0,   469,   472,     0,     0,     0,   477,     0,
       0,   474,     0,     0,   476,     0,     0,     0,     0,     0,
       0,   478,     0,   479,   174,   175,   176,   177,   178,   179,
     180,   181,   182,     0,     0,     0,   186,   187,   188,     0,
       0,     0,     0,   193,   194,   195,   196,   197,     0,     0,
     201,     0,     0,     0,   208,   209,   210,   211,   212,   213,
      -2,     4,     0,   214,   215,   216,   217,   218,     5,     6,
       7,   223,     0,   226,     0,     8,     9,    10,    11,    12,
      13,    14,    15,     0,     0,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    31,    32,    33,    34,     0,     0,     0,     0,     0,
       0,    35,    36,     0,    37,    38,     0,     0,     0,     0,
      39,     0,     0,    40,    41,    42,     0,    43,    44,     0,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   329,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   364,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   436,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   453,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   467,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   475
};

static const short int yycheck[] =
{
       5,    16,     4,     3,     3,     3,    63,     4,     4,    14,
       4,     4,     4,     4,    19,     4,    17,     4,     4,     4,
       4,   154,     4,     4,    29,    30,     4,     4,     4,     4,
      35,    36,     4,     4,    39,     4,   107,    38,     5,     0,
       4,    46,    57,    58,    59,    63,     4,    44,    14,     3,
     107,     9,    68,    69,    11,    12,     3,     3,    24,     3,
      65,    13,    14,    78,     3,    67,     3,     3,     3,     3,
       3,    23,    24,    78,    40,    41,    42,    43,     4,    84,
       3,     3,    79,    49,     3,    50,     3,   220,    40,     3,
       3,    55,    44,    45,    46,    47,    48,    49,    99,    68,
      69,    68,    69,    68,    69,   107,    63,   107,   107,   107,
     107,   107,     3,   107,   107,   107,   107,     3,   107,     3,
     107,   107,   107,   107,   125,   107,   107,    79,     3,   107,
     107,   107,   107,     5,    50,   107,   107,    13,    14,   144,
      66,    67,   107,     3,     3,     3,    71,    23,    24,     3,
       3,   156,    14,    71,     3,   107,     4,    14,    50,     3,
       8,     9,    24,    50,    40,     3,     3,    24,    44,    45,
      46,    47,    48,    49,   189,    23,   191,     3,    40,    41,
      42,    43,     3,    40,    41,    42,    43,    49,     3,     3,
       3,     3,    71,    71,     3,    72,     3,    45,    46,   204,
      48,   206,    49,    79,     4,     6,     3,    49,    56,    57,
      58,    59,    60,     3,     3,     3,     3,    49,     4,     4,
       3,   421,     6,    93,   192,   431,   316,    -1,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,    -1,
      -1,   246,    -1,   248,    -1,   250,    -1,    -1,   253,   254,
      -1,    -1,   257,    -1,   255,    -1,    -1,    -1,   263,   264,
     265,   266,    -1,    -1,   269,   270,    -1,    -1,    -1,    -1,
      -1,   272,   273,    -1,    -1,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,    -1,    -1,
     305,   296,    -1,   298,   299,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   311,    -1,    -1,    -1,
      -1,   312,    -1,    -1,    -1,    -1,   321,   322,    -1,    -1,
      -1,   326,    -1,   328,    -1,    -1,    -1,    -1,   333,   344,
      -1,    -1,    -1,    -1,    -1,   340,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   362,   363,    -1,
      -1,   362,    -1,    -1,   365,    -1,    -1,    -1,    -1,   374,
      -1,   376,    -1,    -1,   389,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   402,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   407,   418,    -1,    -1,    -1,    -1,   413,   410,
      -1,    -1,    -1,   414,   415,   416,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   438,    -1,    -1,    -1,    -1,   443,    -1,
      -1,    -1,   447,    -1,    -1,    -1,   451,   452,    -1,    -1,
      -1,    -1,    -1,   458,   459,    -1,    -1,    -1,   473,    -1,
      -1,   466,    -1,    -1,   469,    -1,    -1,    -1,    -1,    -1,
      -1,   476,    -1,   478,   100,   101,   102,   103,   104,   105,
     106,   107,   108,    -1,    -1,    -1,   112,   113,   114,    -1,
      -1,    -1,    -1,   119,   120,   121,   122,   123,    -1,    -1,
     126,    -1,    -1,    -1,   130,   131,   132,   133,   134,   135,
       0,     1,    -1,   139,   140,   141,   142,   143,     8,     9,
      10,   147,    -1,   149,    -1,    15,    16,    17,    18,    19,
      20,    21,    22,    -1,    -1,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    51,    52,    53,    54,    -1,    -1,    -1,    -1,    -1,
      -1,    61,    62,    -1,    64,    65,    -1,    -1,    -1,    -1,
      70,    -1,    -1,    73,    74,    75,    -1,    77,    78,    -1,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   267,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   313,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   412,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   437,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   454,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   468
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,   109,   110,     0,     1,     8,     9,    10,    15,    16,
      17,    18,    19,    20,    21,    22,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    51,    52,    53,    54,    61,    62,    64,    65,    70,
      73,    74,    75,    77,    78,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   111,   167,   168,   169,   203,   245,   246,
     246,    63,   247,    68,    69,   243,     3,   184,     3,   181,
     158,   159,   160,   161,   246,   141,   243,   247,   140,   246,
     139,   151,   120,   127,   146,   148,   149,   153,   143,   246,
     246,   204,   205,   152,   142,   246,   246,   247,   246,   163,
     164,   162,   145,   144,   128,   246,   131,   133,   118,   119,
     134,   121,   122,   123,   124,   138,   243,   243,   243,   135,
     136,   147,   154,   132,   246,   129,   165,   137,   166,   150,
      50,   170,   243,   246,    50,   171,     5,   156,   246,   185,
     182,     3,   234,     3,   187,     3,   191,   187,   157,     3,
     219,     3,   207,   247,   219,   219,   219,   219,   219,   219,
     219,   219,   219,    71,    71,     3,   219,   219,   219,    50,
     125,    50,   126,   219,   219,   219,   219,   219,     3,   210,
     247,   219,     3,   222,     3,   217,     3,   218,   219,   219,
     219,   219,   219,   219,   219,   219,   219,   219,   219,   246,
       3,     3,   197,   219,     3,   200,   219,   172,   172,   246,
       3,   240,     4,     8,     9,    23,    45,    46,    48,    56,
      57,    58,    59,    60,   186,     4,    55,   183,   235,   188,
     192,     3,   237,   220,   208,    71,    71,   206,   243,     3,
     178,   243,   178,   211,   223,   246,   246,   117,   172,   198,
     201,    14,    24,    40,    41,    42,    43,    49,   173,    49,
      49,   241,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,     4,   236,   246,     4,    66,    67,
     189,     4,    67,   193,   246,   238,     4,   221,   246,     4,
     209,   246,   247,   116,     4,   246,   179,     4,   212,   246,
       4,    44,    79,   224,   246,     4,   246,     4,   246,   219,
     130,     4,   199,   246,     4,   202,   246,   247,   247,   174,
     176,   246,     4,   242,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,     4,
     239,   243,   246,   247,   219,    72,     4,   180,   245,   213,
     246,   246,   225,     4,   246,     4,   246,     4,   246,    13,
      14,    23,    24,    40,    44,    45,    46,    47,    48,    49,
      79,   175,    13,    14,    23,    24,    40,    44,    45,    46,
      47,    48,    49,    79,   177,   246,     6,     5,   243,   190,
     246,   247,   112,   246,    11,    12,   244,   247,    49,     3,
     214,   226,   227,     3,   228,     4,   246,     4,   246,   243,
     243,   155,   246,     3,   194,   247,   219,   113,   246,   247,
     247,   247,   243,   215,   228,     3,   231,   229,     4,     4,
     240,    49,   195,   219,   114,   246,     4,   216,   246,   232,
       4,   230,   246,   246,     4,   196,   246,   219,   115,   246,
       4,   233,   246,     6,   246,   219,   246,   243,   246,   246
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)		\
   ((Current).first_line   = (Rhs)[1].first_line,	\
    (Current).first_column = (Rhs)[1].first_column,	\
    (Current).last_line    = (Rhs)[N].last_line,	\
    (Current).last_column  = (Rhs)[N].last_column)
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

# define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
# define YYDSYMPRINTF(Title, Token, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if defined (YYMAXDEPTH) && YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yytype, yyvaluep)
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  register short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;


  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 10:
#line 166 "gram.y"
    {
		      (void) AddIconRegion(yyvsp[-4].ptr, yyvsp[-3].num, yyvsp[-2].num, yyvsp[-1].num, yyvsp[0].num, "undef", "undef", "undef");
		  }
    break;

  case 11:
#line 169 "gram.y"
    {
		      (void) AddIconRegion(yyvsp[-5].ptr, yyvsp[-4].num, yyvsp[-3].num, yyvsp[-2].num, yyvsp[-1].num, yyvsp[0].ptr, "undef", "undef");
		  }
    break;

  case 12:
#line 172 "gram.y"
    {
		      (void) AddIconRegion(yyvsp[-6].ptr, yyvsp[-5].num, yyvsp[-4].num, yyvsp[-3].num, yyvsp[-2].num, yyvsp[-1].ptr, yyvsp[0].ptr, "undef");
		  }
    break;

  case 13:
#line 175 "gram.y"
    {
		      (void) AddIconRegion(yyvsp[-7].ptr, yyvsp[-6].num, yyvsp[-5].num, yyvsp[-4].num, yyvsp[-3].num, yyvsp[-2].ptr, yyvsp[-1].ptr, yyvsp[0].ptr);
		  }
    break;

  case 14:
#line 178 "gram.y"
    {
		      list = AddIconRegion(yyvsp[-4].ptr, yyvsp[-3].num, yyvsp[-2].num, yyvsp[-1].num, yyvsp[0].num, "undef", "undef", "undef");
		  }
    break;

  case 16:
#line 182 "gram.y"
    {
		      list = AddIconRegion(yyvsp[-5].ptr, yyvsp[-4].num, yyvsp[-3].num, yyvsp[-2].num, yyvsp[-1].num, yyvsp[0].ptr, "undef", "undef");
		  }
    break;

  case 18:
#line 186 "gram.y"
    {
		      list = AddIconRegion(yyvsp[-6].ptr, yyvsp[-5].num, yyvsp[-4].num, yyvsp[-3].num, yyvsp[-2].num, yyvsp[-1].ptr, yyvsp[0].ptr, "undef");
		  }
    break;

  case 20:
#line 190 "gram.y"
    {
		      list = AddIconRegion(yyvsp[-7].ptr, yyvsp[-6].num, yyvsp[-5].num, yyvsp[-4].num, yyvsp[-3].num, yyvsp[-2].ptr, yyvsp[-1].ptr, yyvsp[0].ptr);
		  }
    break;

  case 22:
#line 195 "gram.y"
    {
		      list = AddWindowRegion (yyvsp[-2].ptr, yyvsp[-1].num, yyvsp[0].num);
		  }
    break;

  case 24:
#line 200 "gram.y"
    {
		      list = addWindowBox (yyvsp[-1].ptr, yyvsp[0].ptr);
		  }
    break;

  case 26:
#line 205 "gram.y"
    { if (Scr->FirstTime)
						  {
						    Scr->iconmgr->geometry= (char*)yyvsp[-1].ptr;
						    Scr->iconmgr->columns=yyvsp[0].num;
						  }
						}
    break;

  case 27:
#line 211 "gram.y"
    { if (Scr->FirstTime)
						    Scr->iconmgr->geometry = (char*)yyvsp[0].ptr;
						}
    break;

  case 28:
#line 214 "gram.y"
    { if (Scr->FirstTime)
				{
				    Scr->workSpaceMgr.geometry= (char*)yyvsp[-1].ptr;
				    Scr->workSpaceMgr.columns=yyvsp[0].num;
				}
						}
    break;

  case 29:
#line 220 "gram.y"
    { if (Scr->FirstTime)
				    Scr->workSpaceMgr.geometry = (char*)yyvsp[0].ptr;
						}
    break;

  case 30:
#line 223 "gram.y"
    {}
    break;

  case 32:
#line 226 "gram.y"
    {}
    break;

  case 34:
#line 229 "gram.y"
    { if (Scr->FirstTime)
					  {
						Scr->DoZoom = TRUE;
						Scr->ZoomCount = yyvsp[0].num;
					  }
					}
    break;

  case 35:
#line 235 "gram.y"
    { if (Scr->FirstTime)
						Scr->DoZoom = TRUE; }
    break;

  case 36:
#line 237 "gram.y"
    {}
    break;

  case 37:
#line 238 "gram.y"
    {}
    break;

  case 38:
#line 239 "gram.y"
    { list = &Scr->IconifyByUn; }
    break;

  case 40:
#line 241 "gram.y"
    { if (Scr->FirstTime)
		    Scr->IconifyByUnmapping = TRUE; }
    break;

  case 41:
#line 244 "gram.y"
    { list = &Scr->OpaqueMoveList; }
    break;

  case 43:
#line 246 "gram.y"
    { if (Scr->FirstTime) Scr->DoOpaqueMove = TRUE; }
    break;

  case 44:
#line 247 "gram.y"
    { list = &Scr->NoOpaqueMoveList; }
    break;

  case 46:
#line 249 "gram.y"
    { if (Scr->FirstTime) Scr->DoOpaqueMove = FALSE; }
    break;

  case 47:
#line 250 "gram.y"
    { list = &Scr->OpaqueMoveList; }
    break;

  case 49:
#line 252 "gram.y"
    { if (Scr->FirstTime) Scr->DoOpaqueResize = TRUE; }
    break;

  case 50:
#line 253 "gram.y"
    { list = &Scr->NoOpaqueResizeList; }
    break;

  case 52:
#line 255 "gram.y"
    { if (Scr->FirstTime) Scr->DoOpaqueResize = FALSE; }
    break;

  case 53:
#line 257 "gram.y"
    {
					  GotTitleButton (yyvsp[-2].ptr, yyvsp[0].num, False);
					}
    break;

  case 54:
#line 260 "gram.y"
    {
					  GotTitleButton (yyvsp[-2].ptr, yyvsp[0].num, True);
					}
    break;

  case 55:
#line 263 "gram.y"
    { CreateTitleButton(yyvsp[0].ptr, 0, NULL, NULL, FALSE, TRUE); }
    break;

  case 57:
#line 265 "gram.y"
    { CreateTitleButton(yyvsp[0].ptr, 0, NULL, NULL, TRUE, TRUE); }
    break;

  case 59:
#line 267 "gram.y"
    {
		    root = GetRoot(yyvsp[0].ptr, NULLSTR, NULLSTR);
		    AddFuncButton (yyvsp[-1].num, C_ROOT, 0, F_MENU, root, (MenuItem*) 0);
		}
    break;

  case 60:
#line 271 "gram.y"
    {
			if (yyvsp[0].num == F_MENU) {
			    pull->prev = NULL;
			    AddFuncButton (yyvsp[-1].num, C_ROOT, 0, yyvsp[0].num, pull, (MenuItem*) 0);
			}
			else {
			    MenuItem *item;

			    root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
			    item = AddToMenu (root, "x", Action,
					NULL, yyvsp[0].num, NULLSTR, NULLSTR);
			    AddFuncButton (yyvsp[-1].num, C_ROOT, 0, yyvsp[0].num, (MenuRoot*) 0, item);
			}
			Action = "";
			pull = NULL;
		}
    break;

  case 61:
#line 287 "gram.y"
    { GotKey(yyvsp[-1].ptr, yyvsp[0].num); }
    break;

  case 62:
#line 288 "gram.y"
    { GotButton(yyvsp[-1].num, yyvsp[0].num); }
    break;

  case 63:
#line 290 "gram.y"
    { list = &Scr->DontIconify; }
    break;

  case 65:
#line 292 "gram.y"
    {}
    break;

  case 67:
#line 294 "gram.y"
    {}
    break;

  case 68:
#line 294 "gram.y"
    { Scr->IgnoreModifier = mods; mods = 0; }
    break;

  case 70:
#line 295 "gram.y"
    { list = &Scr->OccupyAll; }
    break;

  case 72:
#line 297 "gram.y"
    { list = &Scr->IconMenuDontShow; }
    break;

  case 74:
#line 299 "gram.y"
    {}
    break;

  case 76:
#line 301 "gram.y"
    { list = &Scr->UnmapByMovingFarAway; }
    break;

  case 78:
#line 303 "gram.y"
    { list = &Scr->AutoSqueeze; }
    break;

  case 80:
#line 305 "gram.y"
    { list = &Scr->StartSqueezed; }
    break;

  case 82:
#line 307 "gram.y"
    { Scr->AlwaysSqueezeToGravity = TRUE; }
    break;

  case 83:
#line 308 "gram.y"
    { list = &Scr->AlwaysSqueezeToGravityL; }
    break;

  case 85:
#line 310 "gram.y"
    { list = &Scr->DontSetInactive; }
    break;

  case 87:
#line 312 "gram.y"
    { list = &Scr->IconMgrNoShow; }
    break;

  case 89:
#line 314 "gram.y"
    { Scr->IconManagerDontShow = TRUE; }
    break;

  case 90:
#line 315 "gram.y"
    { list = &Scr->IconMgrs; }
    break;

  case 92:
#line 317 "gram.y"
    { list = &Scr->IconMgrShow; }
    break;

  case 94:
#line 319 "gram.y"
    { list = &Scr->NoTitleHighlight; }
    break;

  case 96:
#line 321 "gram.y"
    { if (Scr->FirstTime)
						Scr->TitleHighlight = FALSE; }
    break;

  case 97:
#line 323 "gram.y"
    { list = &Scr->NoHighlight; }
    break;

  case 99:
#line 325 "gram.y"
    { if (Scr->FirstTime)
						Scr->Highlight = FALSE; }
    break;

  case 100:
#line 327 "gram.y"
    { list = &Scr->AlwaysOnTopL; }
    break;

  case 102:
#line 329 "gram.y"
    { list = &Scr->NoStackModeL; }
    break;

  case 104:
#line 331 "gram.y"
    { if (Scr->FirstTime)
						Scr->StackMode = FALSE; }
    break;

  case 105:
#line 333 "gram.y"
    { list = &Scr->NoBorder; }
    break;

  case 107:
#line 335 "gram.y"
    { list = &Scr->DontSave; }
    break;

  case 109:
#line 337 "gram.y"
    { list = &Scr->NoIconTitle; }
    break;

  case 111:
#line 339 "gram.y"
    { if (Scr->FirstTime)
						Scr->NoIconTitlebar = TRUE; }
    break;

  case 112:
#line 341 "gram.y"
    { list = &Scr->NoTitle; }
    break;

  case 114:
#line 343 "gram.y"
    { if (Scr->FirstTime)
						Scr->NoTitlebar = TRUE; }
    break;

  case 115:
#line 345 "gram.y"
    { list = &Scr->IgnoreTransientL; }
    break;

  case 117:
#line 347 "gram.y"
    { donttoggleworkspacemanagerstate = TRUE; }
    break;

  case 118:
#line 348 "gram.y"
    { list = &Scr->MakeTitle; }
    break;

  case 120:
#line 350 "gram.y"
    { list = &Scr->StartIconified; }
    break;

  case 122:
#line 352 "gram.y"
    { list = &Scr->AutoRaise; }
    break;

  case 124:
#line 354 "gram.y"
    { Scr->AutoRaiseDefault = TRUE; }
    break;

  case 125:
#line 355 "gram.y"
    { list = &Scr->AutoLower; }
    break;

  case 127:
#line 357 "gram.y"
    { Scr->AutoLowerDefault = TRUE; }
    break;

  case 128:
#line 358 "gram.y"
    {
					root = GetRoot(yyvsp[-5].ptr, yyvsp[-3].ptr, yyvsp[-1].ptr); }
    break;

  case 129:
#line 360 "gram.y"
    { root->real_menu = TRUE;}
    break;

  case 130:
#line 361 "gram.y"
    { root = GetRoot(yyvsp[0].ptr, NULLSTR, NULLSTR); }
    break;

  case 131:
#line 362 "gram.y"
    { root->real_menu = TRUE; }
    break;

  case 132:
#line 363 "gram.y"
    { root = GetRoot(yyvsp[0].ptr, NULLSTR, NULLSTR); }
    break;

  case 134:
#line 365 "gram.y"
    { list = &Scr->IconNames; }
    break;

  case 136:
#line 367 "gram.y"
    { color = COLOR; }
    break;

  case 138:
#line 369 "gram.y"
    {}
    break;

  case 140:
#line 371 "gram.y"
    { color = MONOCHROME; }
    break;

  case 142:
#line 373 "gram.y"
    { Scr->DefaultFunction.func = yyvsp[0].num;
					  if (yyvsp[0].num == F_MENU)
					  {
					    pull->prev = NULL;
					    Scr->DefaultFunction.menu = pull;
					  }
					  else
					  {
					    root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					    Scr->DefaultFunction.item =
						AddToMenu(root,"x",Action,
							  NULL,yyvsp[0].num, NULLSTR, NULLSTR);
					  }
					  Action = "";
					  pull = NULL;
					}
    break;

  case 143:
#line 389 "gram.y"
    { Scr->WindowFunction.func = yyvsp[0].num;
					   root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					   Scr->WindowFunction.item =
						AddToMenu(root,"x",Action,
							  NULL,yyvsp[0].num, NULLSTR, NULLSTR);
					   Action = "";
					   pull = NULL;
					}
    break;

  case 144:
#line 397 "gram.y"
    { Scr->ChangeWorkspaceFunction.func = yyvsp[0].num;
					   root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					   Scr->ChangeWorkspaceFunction.item =
						AddToMenu(root,"x",Action,
							  NULL,yyvsp[0].num, NULLSTR, NULLSTR);
					   Action = "";
					   pull = NULL;
					}
    break;

  case 145:
#line 405 "gram.y"
    { Scr->DeIconifyFunction.func = yyvsp[0].num;
					   root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					   Scr->DeIconifyFunction.item =
						AddToMenu(root,"x",Action,
							  NULL,yyvsp[0].num, NULLSTR, NULLSTR);
					   Action = "";
					   pull = NULL;
					}
    break;

  case 146:
#line 413 "gram.y"
    { Scr->IconifyFunction.func = yyvsp[0].num;
					   root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					   Scr->IconifyFunction.item =
						AddToMenu(root,"x",Action,
							  NULL,yyvsp[0].num, NULLSTR, NULLSTR);
					   Action = "";
					   pull = NULL;
					}
    break;

  case 147:
#line 421 "gram.y"
    { list = &Scr->WarpCursorL; }
    break;

  case 149:
#line 423 "gram.y"
    { if (Scr->FirstTime)
					    Scr->WarpCursor = TRUE; }
    break;

  case 150:
#line 425 "gram.y"
    { list = &Scr->WindowRingL; }
    break;

  case 152:
#line 427 "gram.y"
    { Scr->WindowRingAll = TRUE; }
    break;

  case 153:
#line 428 "gram.y"
    { if (!Scr->WindowRingL)
					    Scr->WindowRingAll = TRUE;
					  list = &Scr->WindowRingExcludeL; }
    break;

  case 155:
#line 432 "gram.y"
    {  }
    break;

  case 157:
#line 434 "gram.y"
    { }
    break;

  case 159:
#line 437 "gram.y"
    { if (!do_single_keyword (yyvsp[0].num)) {
					    twmrc_error_prefix();
					    fprintf (stderr,
					"unknown singleton keyword %d\n",
						     yyvsp[0].num);
					    ParseError = 1;
					  }
					}
    break;

  case 160:
#line 447 "gram.y"
    { if (!do_string_keyword (yyvsp[-1].num, yyvsp[0].ptr)) {
					    twmrc_error_prefix();
					    fprintf (stderr,
				"unknown string keyword %d (value \"%s\")\n",
						     yyvsp[-1].num, yyvsp[0].ptr);
					    ParseError = 1;
					  }
					}
    break;

  case 161:
#line 455 "gram.y"
    { if (!do_string_keyword (yyvsp[0].num, defstring)) {
					    twmrc_error_prefix();
					    fprintf (stderr,
				"unknown string keyword %d (no value)\n",
						     yyvsp[0].num);
					    ParseError = 1;
					  }
					}
    break;

  case 162:
#line 465 "gram.y"
    { if (!do_number_keyword (yyvsp[-1].num, yyvsp[0].num)) {
					    twmrc_error_prefix();
					    fprintf (stderr,
				"unknown numeric keyword %d (value %d)\n",
						     yyvsp[-1].num, yyvsp[0].num);
					    ParseError = 1;
					  }
					}
    break;

  case 163:
#line 477 "gram.y"
    { yyval.num = yyvsp[0].num; }
    break;

  case 164:
#line 480 "gram.y"
    { yyval.num = yyvsp[0].num; }
    break;

  case 167:
#line 487 "gram.y"
    { mods |= Mod1Mask; }
    break;

  case 168:
#line 488 "gram.y"
    { mods |= ShiftMask; }
    break;

  case 169:
#line 489 "gram.y"
    { mods |= LockMask; }
    break;

  case 170:
#line 490 "gram.y"
    { mods |= ControlMask; }
    break;

  case 171:
#line 491 "gram.y"
    { if (yyvsp[0].num < 1 || yyvsp[0].num > 5) {
					     twmrc_error_prefix();
					     fprintf (stderr,
				"bad modifier number (%d), must be 1-5\n",
						      yyvsp[0].num);
					     ParseError = 1;
					  } else {
					     mods |= (Alt1Mask << (yyvsp[0].num - 1));
					  }
					}
    break;

  case 172:
#line 501 "gram.y"
    { if (yyvsp[0].num < 1 || yyvsp[0].num > 5) {
					     twmrc_error_prefix();
					     fprintf (stderr,
				"bad modifier number (%d), must be 1-5\n",
						      yyvsp[0].num);
					     ParseError = 1;
					  } else {
					     mods |= (Mod1Mask << (yyvsp[0].num - 1));
					  }
					}
    break;

  case 173:
#line 511 "gram.y"
    { }
    break;

  case 176:
#line 518 "gram.y"
    { cont |= C_WINDOW_BIT; }
    break;

  case 177:
#line 519 "gram.y"
    { cont |= C_TITLE_BIT; }
    break;

  case 178:
#line 520 "gram.y"
    { cont |= C_ICON_BIT; }
    break;

  case 179:
#line 521 "gram.y"
    { cont |= C_ROOT_BIT; }
    break;

  case 180:
#line 522 "gram.y"
    { cont |= C_FRAME_BIT; }
    break;

  case 181:
#line 523 "gram.y"
    { cont |= C_WORKSPACE_BIT; }
    break;

  case 182:
#line 524 "gram.y"
    { cont |= C_ICONMGR_BIT; }
    break;

  case 183:
#line 525 "gram.y"
    { cont |= C_ICONMGR_BIT; }
    break;

  case 184:
#line 526 "gram.y"
    { cont |= C_ALTER_BIT; }
    break;

  case 185:
#line 527 "gram.y"
    { cont |= C_ALL_BITS; }
    break;

  case 186:
#line 528 "gram.y"
    {  }
    break;

  case 189:
#line 535 "gram.y"
    { cont |= C_WINDOW_BIT; }
    break;

  case 190:
#line 536 "gram.y"
    { cont |= C_TITLE_BIT; }
    break;

  case 191:
#line 537 "gram.y"
    { cont |= C_ICON_BIT; }
    break;

  case 192:
#line 538 "gram.y"
    { cont |= C_ROOT_BIT; }
    break;

  case 193:
#line 539 "gram.y"
    { cont |= C_FRAME_BIT; }
    break;

  case 194:
#line 540 "gram.y"
    { cont |= C_WORKSPACE_BIT; }
    break;

  case 195:
#line 541 "gram.y"
    { cont |= C_ICONMGR_BIT; }
    break;

  case 196:
#line 542 "gram.y"
    { cont |= C_ICONMGR_BIT; }
    break;

  case 197:
#line 543 "gram.y"
    { cont |= C_ALL_BITS; }
    break;

  case 198:
#line 544 "gram.y"
    { cont |= C_ALTER_BIT; }
    break;

  case 199:
#line 545 "gram.y"
    { }
    break;

  case 200:
#line 546 "gram.y"
    { Name = (char*)yyvsp[0].ptr; cont |= C_NAME_BIT; }
    break;

  case 201:
#line 550 "gram.y"
    {}
    break;

  case 204:
#line 557 "gram.y"
    { ModifyCurrentTB(yyvsp[-2].num, yyvsp[0].num, Action, pull);}
    break;

  case 205:
#line 561 "gram.y"
    {}
    break;

  case 208:
#line 568 "gram.y"
    { SetHighlightPixmap (yyvsp[0].ptr); }
    break;

  case 209:
#line 572 "gram.y"
    {}
    break;

  case 212:
#line 579 "gram.y"
    {
			NewBitmapCursor(&Scr->FrameCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 213:
#line 581 "gram.y"
    {
			NewFontCursor(&Scr->FrameCursor, yyvsp[0].ptr); }
    break;

  case 214:
#line 583 "gram.y"
    {
			NewBitmapCursor(&Scr->TitleCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 215:
#line 585 "gram.y"
    {
			NewFontCursor(&Scr->TitleCursor, yyvsp[0].ptr); }
    break;

  case 216:
#line 587 "gram.y"
    {
			NewBitmapCursor(&Scr->IconCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 217:
#line 589 "gram.y"
    {
			NewFontCursor(&Scr->IconCursor, yyvsp[0].ptr); }
    break;

  case 218:
#line 591 "gram.y"
    {
			NewBitmapCursor(&Scr->IconMgrCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 219:
#line 593 "gram.y"
    {
			NewFontCursor(&Scr->IconMgrCursor, yyvsp[0].ptr); }
    break;

  case 220:
#line 595 "gram.y"
    {
			NewBitmapCursor(&Scr->ButtonCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 221:
#line 597 "gram.y"
    {
			NewFontCursor(&Scr->ButtonCursor, yyvsp[0].ptr); }
    break;

  case 222:
#line 599 "gram.y"
    {
			NewBitmapCursor(&Scr->MoveCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 223:
#line 601 "gram.y"
    {
			NewFontCursor(&Scr->MoveCursor, yyvsp[0].ptr); }
    break;

  case 224:
#line 603 "gram.y"
    {
			NewBitmapCursor(&Scr->ResizeCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 225:
#line 605 "gram.y"
    {
			NewFontCursor(&Scr->ResizeCursor, yyvsp[0].ptr); }
    break;

  case 226:
#line 607 "gram.y"
    {
			NewBitmapCursor(&Scr->WaitCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 227:
#line 609 "gram.y"
    {
			NewFontCursor(&Scr->WaitCursor, yyvsp[0].ptr); }
    break;

  case 228:
#line 611 "gram.y"
    {
			NewBitmapCursor(&Scr->MenuCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 229:
#line 613 "gram.y"
    {
			NewFontCursor(&Scr->MenuCursor, yyvsp[0].ptr); }
    break;

  case 230:
#line 615 "gram.y"
    {
			NewBitmapCursor(&Scr->SelectCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 231:
#line 617 "gram.y"
    {
			NewFontCursor(&Scr->SelectCursor, yyvsp[0].ptr); }
    break;

  case 232:
#line 619 "gram.y"
    {
			NewBitmapCursor(&Scr->DestroyCursor, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 233:
#line 621 "gram.y"
    {
			NewFontCursor(&Scr->DestroyCursor, yyvsp[0].ptr); }
    break;

  case 234:
#line 625 "gram.y"
    {}
    break;

  case 237:
#line 633 "gram.y"
    { if (!do_colorlist_keyword (yyvsp[-1].num, color,
								     yyvsp[0].ptr)) {
					    twmrc_error_prefix();
					    fprintf (stderr,
			"unhandled list color keyword %d (string \"%s\")\n",
						     yyvsp[-1].num, yyvsp[0].ptr);
					    ParseError = 1;
					  }
					}
    break;

  case 238:
#line 642 "gram.y"
    { list = do_colorlist_keyword(yyvsp[-1].num,color,
								      yyvsp[0].ptr);
					  if (!list) {
					    twmrc_error_prefix();
					    fprintf (stderr,
			"unhandled color list keyword %d (string \"%s\")\n",
						     yyvsp[-1].num, yyvsp[0].ptr);
					    ParseError = 1;
					  }
					}
    break;

  case 240:
#line 653 "gram.y"
    { if (!do_color_keyword (yyvsp[-1].num, color,
								 yyvsp[0].ptr)) {
					    twmrc_error_prefix();
					    fprintf (stderr,
			"unhandled color keyword %d (string \"%s\")\n",
						     yyvsp[-1].num, yyvsp[0].ptr);
					    ParseError = 1;
					  }
					}
    break;

  case 241:
#line 664 "gram.y"
    {}
    break;

  case 244:
#line 671 "gram.y"
    { do_string_savecolor(color, yyvsp[0].ptr); }
    break;

  case 245:
#line 672 "gram.y"
    { do_var_savecolor(yyvsp[0].num); }
    break;

  case 246:
#line 675 "gram.y"
    {}
    break;

  case 249:
#line 682 "gram.y"
    { if (Scr->FirstTime &&
					      color == Scr->Monochrome)
					    AddToList(list, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 250:
#line 687 "gram.y"
    {}
    break;

  case 253:
#line 694 "gram.y"
    { AddToList (&Scr->WindowGeometries, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 254:
#line 697 "gram.y"
    {}
    break;

  case 257:
#line 704 "gram.y"
    { AddToList (&Scr->VirtualScreens, yyvsp[0].ptr, ""); }
    break;

  case 258:
#line 707 "gram.y"
    {
				    if (HasShape) Scr->SqueezeTitle = TRUE;
				}
    break;

  case 259:
#line 710 "gram.y"
    { list = &Scr->SqueezeTitleL;
				  if (HasShape && Scr->SqueezeTitle == -1)
				    Scr->SqueezeTitle = TRUE;
				}
    break;

  case 261:
#line 715 "gram.y"
    { Scr->SqueezeTitle = FALSE; }
    break;

  case 262:
#line 716 "gram.y"
    { list = &Scr->DontSqueezeTitleL; }
    break;

  case 265:
#line 721 "gram.y"
    {
				if (Scr->FirstTime) {
				   do_squeeze_entry (list, yyvsp[-3].ptr, yyvsp[-2].num, yyvsp[-1].num, yyvsp[0].num);
				}
			}
    break;

  case 266:
#line 729 "gram.y"
    {}
    break;

  case 269:
#line 736 "gram.y"
    { if (Scr->FirstTime)
					    AddToList(list, yyvsp[-2].ptr, (char *)
						AllocateIconManager(yyvsp[-2].ptr, NULLSTR,
							yyvsp[-1].ptr,yyvsp[0].num));
					}
    break;

  case 270:
#line 742 "gram.y"
    { if (Scr->FirstTime)
					    AddToList(list, yyvsp[-3].ptr, (char *)
						AllocateIconManager(yyvsp[-3].ptr,yyvsp[-2].ptr,
						yyvsp[-1].ptr, yyvsp[0].num));
					}
    break;

  case 271:
#line 749 "gram.y"
    {}
    break;

  case 274:
#line 756 "gram.y"
    {
			AddWorkSpace (yyvsp[0].ptr, NULLSTR, NULLSTR, NULLSTR, NULLSTR, NULLSTR);
		}
    break;

  case 275:
#line 759 "gram.y"
    {
			curWorkSpc = (char*)yyvsp[0].ptr;
		}
    break;

  case 277:
#line 765 "gram.y"
    {}
    break;

  case 280:
#line 772 "gram.y"
    {
			AddWorkSpace (curWorkSpc, yyvsp[0].ptr, NULLSTR, NULLSTR, NULLSTR, NULLSTR);
		}
    break;

  case 281:
#line 775 "gram.y"
    {
			AddWorkSpace (curWorkSpc, yyvsp[-1].ptr, yyvsp[0].ptr, NULLSTR, NULLSTR, NULLSTR);
		}
    break;

  case 282:
#line 778 "gram.y"
    {
			AddWorkSpace (curWorkSpc, yyvsp[-2].ptr, yyvsp[-1].ptr, yyvsp[0].ptr, NULLSTR, NULLSTR);
		}
    break;

  case 283:
#line 781 "gram.y"
    {
			AddWorkSpace (curWorkSpc, yyvsp[-3].ptr, yyvsp[-2].ptr, yyvsp[-1].ptr, yyvsp[0].ptr, NULLSTR);
		}
    break;

  case 284:
#line 784 "gram.y"
    {
			AddWorkSpace (curWorkSpc, yyvsp[-4].ptr, yyvsp[-3].ptr, yyvsp[-2].ptr, yyvsp[-1].ptr, yyvsp[0].ptr);
		}
    break;

  case 285:
#line 789 "gram.y"
    {
		    WMapCreateCurrentBackGround (yyvsp[-1].ptr, NULL, NULL, NULL);
		}
    break;

  case 286:
#line 792 "gram.y"
    {
		    WMapCreateCurrentBackGround (yyvsp[-2].ptr, yyvsp[-1].ptr, NULL, NULL);
		}
    break;

  case 287:
#line 795 "gram.y"
    {
		    WMapCreateCurrentBackGround (yyvsp[-3].ptr, yyvsp[-2].ptr, yyvsp[-1].ptr, NULL);
		}
    break;

  case 288:
#line 798 "gram.y"
    {
		    WMapCreateCurrentBackGround (yyvsp[-4].ptr, yyvsp[-3].ptr, yyvsp[-2].ptr, yyvsp[-1].ptr);
		}
    break;

  case 289:
#line 803 "gram.y"
    {
		    WMapCreateDefaultBackGround (yyvsp[-1].ptr, NULL, NULL, NULL);
		}
    break;

  case 290:
#line 806 "gram.y"
    {
		    WMapCreateDefaultBackGround (yyvsp[-2].ptr, yyvsp[-1].ptr, NULL, NULL);
		}
    break;

  case 291:
#line 809 "gram.y"
    {
		    WMapCreateDefaultBackGround (yyvsp[-3].ptr, yyvsp[-2].ptr, yyvsp[-1].ptr, NULL);
		}
    break;

  case 292:
#line 812 "gram.y"
    {
		    WMapCreateDefaultBackGround (yyvsp[-4].ptr, yyvsp[-3].ptr, yyvsp[-2].ptr, yyvsp[-1].ptr);
		}
    break;

  case 293:
#line 817 "gram.y"
    {}
    break;

  case 296:
#line 824 "gram.y"
    { if (Scr->FirstTime)
					    AddToList(list, yyvsp[0].ptr, 0);
					}
    break;

  case 297:
#line 829 "gram.y"
    {}
    break;

  case 300:
#line 836 "gram.y"
    {client = (char*)yyvsp[0].ptr;}
    break;

  case 302:
#line 838 "gram.y"
    {client = (char*)yyvsp[0].ptr;}
    break;

  case 304:
#line 840 "gram.y"
    {workspace = (char*)yyvsp[0].ptr;}
    break;

  case 306:
#line 844 "gram.y"
    {}
    break;

  case 309:
#line 851 "gram.y"
    {
				AddToClientsList (yyvsp[0].ptr, client);
			  }
    break;

  case 310:
#line 856 "gram.y"
    {}
    break;

  case 313:
#line 863 "gram.y"
    {
				AddToClientsList (workspace, yyvsp[0].ptr);
			  }
    break;

  case 314:
#line 867 "gram.y"
    {}
    break;

  case 317:
#line 874 "gram.y"
    { if (Scr->FirstTime) AddToList(list, yyvsp[-1].ptr, yyvsp[0].ptr); }
    break;

  case 318:
#line 877 "gram.y"
    {}
    break;

  case 321:
#line 884 "gram.y"
    { AddToMenu(root, "", Action, NULL, yyvsp[0].num,
						    NULLSTR, NULLSTR);
					  Action = "";
					}
    break;

  case 322:
#line 890 "gram.y"
    {lastmenuitem = (MenuItem*) 0;}
    break;

  case 325:
#line 897 "gram.y"
    {
			if (yyvsp[0].num == F_SEPARATOR) {
			    if (lastmenuitem) lastmenuitem->separated = 1;
			}
			else {
			    lastmenuitem = AddToMenu(root, yyvsp[-1].ptr, Action, pull, yyvsp[0].num, NULLSTR, NULLSTR);
			    Action = "";
			    pull = NULL;
			}
		}
    break;

  case 326:
#line 907 "gram.y"
    {
			if (yyvsp[0].num == F_SEPARATOR) {
			    if (lastmenuitem) lastmenuitem->separated = 1;
			}
			else {
			    lastmenuitem = AddToMenu(root, yyvsp[-6].ptr, Action, pull, yyvsp[0].num, yyvsp[-4].ptr, yyvsp[-2].ptr);
			    Action = "";
			    pull = NULL;
			}
		}
    break;

  case 327:
#line 919 "gram.y"
    { yyval.num = yyvsp[0].num; }
    break;

  case 328:
#line 920 "gram.y"
    {
				yyval.num = yyvsp[-1].num;
				Action = (char*)yyvsp[0].ptr;
				switch (yyvsp[-1].num) {
				  case F_MENU:
				    pull = GetRoot (yyvsp[0].ptr, NULLSTR,NULLSTR);
				    pull->prev = root;
				    break;
				  case F_WARPRING:
				    if (!CheckWarpRingArg (Action)) {
					twmrc_error_prefix();
					fprintf (stderr,
			"ignoring invalid f.warptoring argument \"%s\"\n",
						 Action);
					yyval.num = F_NOP;
				    }
				  case F_WARPTOSCREEN:
				    if (!CheckWarpScreenArg (Action)) {
					twmrc_error_prefix();
					fprintf (stderr,
			"ignoring invalid f.warptoscreen argument \"%s\"\n",
						 Action);
					yyval.num = F_NOP;
				    }
				    break;
				  case F_COLORMAP:
				    if (CheckColormapArg (Action)) {
					yyval.num = F_COLORMAP;
				    } else {
					twmrc_error_prefix();
					fprintf (stderr,
			"ignoring invalid f.colormap argument \"%s\"\n",
						 Action);
					yyval.num = F_NOP;
				    }
				    break;
				} /* end switch */
				   }
    break;

  case 329:
#line 961 "gram.y"
    { yyval.num = yyvsp[0].num; }
    break;

  case 330:
#line 962 "gram.y"
    { yyval.num = yyvsp[0].num; }
    break;

  case 331:
#line 963 "gram.y"
    { yyval.num = -(yyvsp[0].num); }
    break;

  case 332:
#line 966 "gram.y"
    { yyval.num = yyvsp[0].num;
					  if (yyvsp[0].num == 0)
						yyerror("bad button 0");

					  if (yyvsp[0].num > MAX_BUTTONS)
					  {
						yyval.num = 0;
						yyerror("button number too large");
					  }
					}
    break;

  case 333:
#line 978 "gram.y"
    { ptr = (char *)malloc(strlen((char*)yyvsp[0].ptr)+1);
					  strcpy(ptr, yyvsp[0].ptr);
					  RemoveDQuote(ptr);
					  yyval.ptr = ptr;
					}
    break;

  case 334:
#line 985 "gram.y"
    { yyval.num = yyvsp[0].num; }
    break;


    }

/* Line 1010 of yacc.c.  */
#line 3308 "y.tab.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  const char* yyprefix;
	  char *yymsg;
	  int yyx;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 0;

	  yyprefix = ", expecting ";
	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		yysize += yystrlen (yyprefix) + yystrlen (yytname [yyx]);
		yycount += 1;
		if (yycount == 5)
		  {
		    yysize = 0;
		    break;
		  }
	      }
	  yysize += (sizeof ("syntax error, unexpected ")
		     + yystrlen (yytname[yytype]));
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yyprefix = ", expecting ";
		  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			yyp = yystpcpy (yyp, yyprefix);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yyprefix = " or ";
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* If at end of input, pop the error token,
	     then the rest of the stack, then return failure.  */
	  if (yychar == YYEOF)
	     for (;;)
	       {
		 YYPOPSTACK;
		 if (yyssp == yyss)
		   YYABORT;
		 YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
		 yydestruct (yystos[*yyssp], yyvsp);
	       }
        }
      else
	{
	  YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
	  yydestruct (yytoken, &yylval);
	  yychar = YYEMPTY;

	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

#ifdef __GNUC__
  /* Pacify GCC when the user code never invokes YYERROR and the label
     yyerrorlab therefore never appears in user code.  */
  if (0)
     goto yyerrorlab;
#endif

  yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 988 "gram.y"

void yyerror(char *s)
{
    twmrc_error_prefix();
    fprintf (stderr, "error in input file:  %s\n", s ? s : "");
    ParseError = 1;
}

void RemoveDQuote(char *str)
{
    register char *i, *o;
    register int n;
    register int count;

    for (i=str+1, o=str; *i && *i != '\"'; o++)
    {
	if (*i == '\\')
	{
	    switch (*++i)
	    {
	    case 'n':
		*o = '\n';
		i++;
		break;
	    case 'b':
		*o = '\b';
		i++;
		break;
	    case 'r':
		*o = '\r';
		i++;
		break;
	    case 't':
		*o = '\t';
		i++;
		break;
	    case 'f':
		*o = '\f';
		i++;
		break;
	    case '0':
		if (*++i == 'x')
		    goto hex;
		else
		    --i;
	    case '1': case '2': case '3':
	    case '4': case '5': case '6': case '7':
		n = 0;
		count = 0;
		while (*i >= '0' && *i <= '7' && count < 3)
		{
		    n = (n<<3) + (*i++ - '0');
		    count++;
		}
		*o = n;
		break;
	    hex:
	    case 'x':
		n = 0;
		count = 0;
		while (i++, count++ < 2)
		{
		    if (*i >= '0' && *i <= '9')
			n = (n<<4) + (*i - '0');
		    else if (*i >= 'a' && *i <= 'f')
			n = (n<<4) + (*i - 'a') + 10;
		    else if (*i >= 'A' && *i <= 'F')
			n = (n<<4) + (*i - 'A') + 10;
		    else
			break;
		}
		*o = n;
		break;
	    case '\n':
		i++;	/* punt */
		o--;	/* to account for o++ at end of loop */
		break;
	    case '\"':
	    case '\'':
	    case '\\':
	    default:
		*o = *i++;
		break;
	    }
	}
	else
	    *o = *i++;
    }
    *o = '\0';
}

static MenuRoot *GetRoot(char *name, char *fore, char *back)
{
    MenuRoot *tmp;

    tmp = FindMenuRoot(name);
    if (tmp == NULL)
	tmp = NewMenuRoot(name);

    if (fore)
    {
	int save;

	save = Scr->FirstTime;
	Scr->FirstTime = TRUE;
	GetColor(COLOR, &tmp->highlight.fore, fore);
	GetColor(COLOR, &tmp->highlight.back, back);
	Scr->FirstTime = save;
    }

    return tmp;
}

static void GotButton (int butt, int func)
{
    int i;
    MenuItem *item;

    for (i = 0; i < NUM_CONTEXTS; i++) {
	if ((cont & (1 << i)) == 0) continue;

	if (func == F_MENU) {
	    pull->prev = NULL;
	    AddFuncButton (butt, i, mods, func, pull, (MenuItem*) 0);
	}
	else {
	    root = GetRoot (TWM_ROOT, NULLSTR, NULLSTR);
	    item = AddToMenu (root, "x", Action, NULL, func, NULLSTR, NULLSTR);
	    AddFuncButton (butt, i, mods, func, (MenuRoot*) 0, item);
	}
    }

    Action = "";
    pull = NULL;
    cont = 0;
    mods_used |= mods;
    mods = 0;
}

static void GotKey(char *key, int func)
{
    int i;

    for (i = 0; i < NUM_CONTEXTS; i++)
    {
	if ((cont & (1 << i)) == 0)
	  continue;

	if (func == F_MENU) {
	    pull->prev = NULL;
	    if (!AddFuncKey (key, i, mods, func, pull, Name, Action)) break;
	}
	else
	if (!AddFuncKey(key, i, mods, func, (MenuRoot*) 0, Name, Action))
	  break;
    }

    Action = "";
    pull = NULL;
    cont = 0;
    mods_used |= mods;
    mods = 0;
}


static void GotTitleButton (char *bitmapname, int func, Bool rightside)
{
    if (!CreateTitleButton (bitmapname, func, Action, pull, rightside, True)) {
	twmrc_error_prefix();
	fprintf (stderr,
		 "unable to create %s titlebutton \"%s\"\n",
		 rightside ? "right" : "left", bitmapname);
    }
    Action = "";
    pull = NULL;
}

static Bool CheckWarpScreenArg (register char *s)
{
    XmuCopyISOLatin1Lowered (s, s);

    if (strcmp (s,  WARPSCREEN_NEXT) == 0 ||
	strcmp (s,  WARPSCREEN_PREV) == 0 ||
	strcmp (s,  WARPSCREEN_BACK) == 0)
      return True;

    for (; *s && isascii(*s) && isdigit(*s); s++) ; /* SUPPRESS 530 */
    return (*s ? False : True);
}


static Bool CheckWarpRingArg (register char *s)
{
    XmuCopyISOLatin1Lowered (s, s);

    if (strcmp (s,  WARPSCREEN_NEXT) == 0 ||
	strcmp (s,  WARPSCREEN_PREV) == 0)
      return True;

    return False;
}


static Bool CheckColormapArg (register char *s)
{
    XmuCopyISOLatin1Lowered (s, s);

    if (strcmp (s, COLORMAP_NEXT) == 0 ||
	strcmp (s, COLORMAP_PREV) == 0 ||
	strcmp (s, COLORMAP_DEFAULT) == 0)
      return True;

    return False;
}


void twmrc_error_prefix (void)
{
    fprintf (stderr, "%s:  line %d:  ", ProgramName, twmrc_lineno);
}

